# == Schema Information
#
# Table name: sources
#
#  id         :integer          not null, primary key
#  name       :string
#  url        :string
#  type       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Source < ActiveRecord::Base
  attr_accessible :name, :url, :type
  validates :name, presence: true, length: { maximum: 50 }
  validates :url, presence: true
  validates :type, presence: true

  enum type: { :custom => 0, :rss => 1 }
  enum status: { :inactive => 0, :active => 1}

end
