# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Source.create(name: "Twitter", url: "https://app.twitter.com", type: "custom", status: "active")
Source.create(name: "CNN Top Stories", url: "http://rss.cnn.com/rss/cnn_topstories.rss", type: "rss", status: "inactive")
Source.create(name: "CNN World", url: "http://rss.cnn.com/rss/cnn_world.rss", type: "rss", status: "inactive")
Source.create(name: "CNN US", url: "http://rss.cnn.com/rss/cnn_us.rss", type: "rss", status: "inactive")
Source.create(name: "CNN Politics", url: "http://rss.cnn.com/rss/cnn_allpolitics.rss", type: "rss", status: "inactive")
Source.create(name: "CNN Technology", url: "http://rss.cnn.com/rss/cnn_tech.rss", type: "rss", status: "inactive")

Source.create(name: "POLITICO Magazine", url: "http://rss.cnn.com/rss/cnn_tech.rss", type: "rss", status: "inactive")
Source.create(name: "POLITICO Morning Cybersecurity", url: "http://www.politico.com/rss/morningcybersecurity.xml", type: "rss", status: "inactive")
Source.create(name: "POLITICO Morning Defense", url: "http://www.politico.com/rss/morningdefense.xml", type: "rss", status: "inactive")
Source.create(name: "POLITICO Morning eHealth", url: "http://www.politico.com/rss/morningehealth.xml", type: "rss", status: "inactive")
Source.create(name: "POLITICO Morning Energy", url: "http://www.politico.com/rss/morningenergy.xml", type: "rss", status: "inactive")
Source.create(name: "POLITICO Morning Money", url: "http://www.politico.com/rss/morningmoney.xml", type: "rss", status: "inactive")
Source.create(name: "POLITICO Morning Tech", url: "http://www.politico.com/rss/morningtech.xml", type: "rss", status: "inactive")
Source.create(name: "POLITICO Top Stories", url: "http://www.politico.com/rss/politicopicks.xml", type: "rss", status: "inactive")
